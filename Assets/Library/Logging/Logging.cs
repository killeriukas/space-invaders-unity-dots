﻿
namespace TMI.LogManagement {

    public static class Logging {

        public static ILogOutput output = new UnityLogOutput();

        [System.Diagnostics.Conditional("ENABLE_LOGGING")]
        public static void Log(string typeFullName, string unformattedString, params object[] moreParameters) {
            string formattedString = string.Format(unformattedString, moreParameters);
            output.Log(typeFullName, formattedString);
        }

        [System.Diagnostics.Conditional("ENABLE_LOGGING")]
        public static void Log(ILoggable loggableObject, string unformattedString, params object[] moreParameters) {
            string formattedString = string.Format(unformattedString, moreParameters);
            output.Log(loggableObject, formattedString);
        }

        [System.Diagnostics.Conditional("ENABLE_LOGGING")]
        public static void LogWarning(ILoggable loggableObject, string unformattedString, params object[] moreParameters) {
            string formattedString = string.Format(unformattedString, moreParameters);
            output.LogWarning(loggableObject, formattedString);
        }

        [System.Diagnostics.Conditional("ENABLE_LOGGING")]
        public static void LogWarning(string typeFullName, string unformattedString, params object[] moreParameters) {
            string formattedString = string.Format(unformattedString, moreParameters);
            output.LogWarning(typeFullName, formattedString);
        }

        [System.Diagnostics.Conditional("ENABLE_LOGGING")]
        public static void LogError(ILoggable loggableObject, string unformattedString, params object[] moreParameters) {
            string formattedString = string.Format(unformattedString, moreParameters);
            output.LogError(loggableObject, formattedString);
        }

    }

}