﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[DisableAutoCreation]
public class WaveMovementSystem : SystemBase {

    private float moveDelta = 0f;

    protected override void OnUpdate() {

        float newMoveDelta = moveDelta + Time.DeltaTime * 20f;
        moveDelta = newMoveDelta;

        Entities.ForEach((ref Translation translation) => {
            float3 currentPosition = translation.Value;

            float newX = currentPosition.x + newMoveDelta;

            if(newX > 100f) {
                newX -= 100f;
            }

            float height = Mathf.PerlinNoise(newX / 100f, currentPosition.z / 100f) * 100f - 50f;

            translation.Value = new float3(currentPosition.x, height, currentPosition.z);
        }).ScheduleParallel();

        //Entity tilePrefab = ConverterBehaviour.instance.GetPrefabById("tile_3d");

        //NativeArray<Entity> chunkTilesArray = new NativeArray<Entity>(maxHeight * maxWidth, Allocator.Temp);

        //EntityManager.Instantiate(tilePrefab, chunkTilesArray);

        //for(int i = 0; i < maxHeight; ++i) {
        //    for(int j = 0; j < maxWidth; ++j) {
        //        float height = Mathf.PerlinNoise(j / (float)maxWidth, i / (float)maxHeight) * 100f - 50f;
        //        EntityManager.SetComponentData(chunkTilesArray[j + i * maxWidth], new Translation() { Value = new float3(j, height, i)});
        //    }
        //}

        //Enabled = false;
    }

}
