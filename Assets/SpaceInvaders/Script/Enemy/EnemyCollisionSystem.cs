﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

[CreateAtScene("game_arena")]
public class EnemyCollisionSystem : ECBSystem<EndSimulationEntityCommandBufferSystem> {

    [BurstCompile]
    private struct EnemyCollisionJob : ITriggerEventsJob {

        public EntityCommandBuffer ecb;

        public NativeArray<ScoreData> scoreDataArray;

        [ReadOnly]
        public ComponentDataFromEntity<EnemyShipTag> enemyEntities;

        [ReadOnly]
        public ComponentDataFromEntity<TeamData> teamEntities;

        public void Execute(TriggerEvent triggerEvent) {
            Entity ea = triggerEvent.Entities.EntityA;
            Entity eb = triggerEvent.Entities.EntityB;

            //if at least one of the components don't have team, that means they must collide
            if(teamEntities.HasComponent(ea) && teamEntities.HasComponent(eb)) {

                TeamData.Team teamA = teamEntities[ea].team;
                TeamData.Team teamB = teamEntities[eb].team;

                //ignore bullets hitting the spawner
                if(teamA == teamB) {
                    return;
                }

            }

            if(enemyEntities.HasComponent(ea)) {
                ScoreData scoreData = scoreDataArray[0];
                scoreData.score += 10;
                scoreDataArray[0] = scoreData;
                ecb.DestroyEntity(ea);
            }

            if(enemyEntities.HasComponent(eb)) {
                ScoreData scoreData = scoreDataArray[0];
                scoreData.score += 10;
                scoreDataArray[0] = scoreData;
                ecb.DestroyEntity(eb);
            }

        }

    }

    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate() {
        base.OnCreate();
        buildPhysicsWorld = World.GetExistingSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetExistingSystem<StepPhysicsWorld>();
    }

    protected override void OnUpdate() {

        NativeArray<ScoreData> scoreData = new NativeArray<ScoreData>(1, Allocator.TempJob);
        scoreData[0] = GetSingleton<ScoreData>();

        EnemyCollisionJob enemyCollisionJob = new EnemyCollisionJob();
        enemyCollisionJob.enemyEntities = GetComponentDataFromEntity<EnemyShipTag>(true);
        enemyCollisionJob.teamEntities = GetComponentDataFromEntity<TeamData>(true);
        enemyCollisionJob.scoreDataArray = scoreData;
        enemyCollisionJob.ecb = CreateEntityCommandBuffer();
        JobHandle enemyCollisionHandle = enemyCollisionJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        AddJobHandleForProducer(enemyCollisionHandle);

        Dependency = enemyCollisionHandle;

        //complete the job here, because it crashes for no known reason...
        //maybe it's a bug in the SDK, but need to investigate further
        Dependency.Complete();

        SetSingleton(scoreData[0]);
        scoreData.Dispose();

    }
}
