﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMI.Helper;
using Unity.Entities;
using UnityEngine.SceneManagement;

[UpdateInGroup(typeof(InitializationSystemGroup))]
public class SceneChangeSystem : ECBSystem<BeginInitializationEntityCommandBufferSystem> {

    private Dictionary<ComponentSystemBase, ComponentSystemGroup> systemGroupBySceneSystem = new Dictionary<ComponentSystemBase, ComponentSystemGroup>();

    private List<Type> allCreateAtConcreteSystems;

    protected override void OnCreate() {
        base.OnCreate();
        List<Type> allCreateAtSystems = ReflectionHelper.FindAllClassesWithAttribute<CreateAtSceneAttribute>();
        allCreateAtConcreteSystems = allCreateAtSystems.Where(x => !x.IsAbstract).ToList();

        Scene currentScene = SceneManager.GetActiveScene();
        InitializeSceneSystems(currentScene.name);
    }

    private void InitializeSceneSystems(string sceneName) {
        List<Type> allSceneSystems = FindSceneSystems(allCreateAtConcreteSystems, sceneName);

        foreach(Type type in allSceneSystems) {
            Type systemGroupType;

            object[] uigAttribute = type.GetCustomAttributes(typeof(UpdateInGroupAttribute), false);
            if(uigAttribute.Length > 0) {
                UpdateInGroupAttribute attribute = (UpdateInGroupAttribute)uigAttribute[0];
                systemGroupType = attribute.GroupType;
            } else {
                systemGroupType = typeof(SimulationSystemGroup);
            }

            ComponentSystemBase componentBaseSystem = World.GetExistingSystem(systemGroupType);
            ComponentSystemGroup systemGroup = (ComponentSystemGroup)componentBaseSystem;

            ComponentSystemBase newSystem = World.GetOrCreateSystem(type);
            systemGroup.AddSystemToUpdateList(newSystem);

            systemGroupBySceneSystem.Add(newSystem, systemGroup);
        }

    }

    private static List<Type> FindSceneSystems(List<Type> systems, string sceneName) {
        List<Type> foundTypes = new List<Type>();
        foreach(Type type in systems) {
            object[] attributes = type.GetCustomAttributes(typeof(CreateAtSceneAttribute), false);

            CreateAtSceneAttribute createAtSceneAttribute = (CreateAtSceneAttribute)attributes[0];
            if(createAtSceneAttribute.sceneName.Equals(sceneName)) {
                foundTypes.Add(type);
            }
        }
        return foundTypes;
    }

    public void ChangeScene(string newSceneName) {
        var cmb = CreateEntityCommandBuffer();
        Entity newEntity = cmb.CreateEntity();
        cmb.AddComponent(newEntity, new SceneChangeData() { sceneName = newSceneName });
    }


    protected override void OnUpdate() {
        Entities.WithStructuralChanges().WithoutBurst().ForEach((Entity entity, in SceneChangeData sceneChangeData) => {
            RemoveAllCurrentSceneSystems();

            string newSceneName = sceneChangeData.sceneName.ToString();

            //load into the new scene
            SceneManager.LoadScene(newSceneName, LoadSceneMode.Single);

            //spawn new systems
            InitializeSceneSystems(newSceneName);

            EntityManager.DestroyEntity(entity);
        }).Run();

    }

    private void RemoveAllCurrentSceneSystems() {

        foreach(var system in systemGroupBySceneSystem.Keys) {
            system.Enabled = false;
        }

        foreach(var kvp in systemGroupBySceneSystem) {
            kvp.Value.RemoveSystemFromUpdateList(kvp.Key);
        }

        foreach(var system in systemGroupBySceneSystem.Keys) {
            World.DestroySystem(system);
        }

        systemGroupBySceneSystem.Clear();
    }

}
