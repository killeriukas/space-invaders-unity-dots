﻿using UnityEngine;

public abstract class SingletonBehaviour<Template> : MonoBehaviour where Template : SingletonBehaviour<Template> {

    public static Template instance { get; private set; }

    protected virtual void Awake() {
        instance = (Template)this;
    }

    protected virtual void OnDestroy() {
        instance = null;
    }

}