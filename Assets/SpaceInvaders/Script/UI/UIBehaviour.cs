﻿using TMPro;
using UnityEngine;

public class UIBehaviour : SingletonBehaviour<UIBehaviour> {

    [SerializeField]
    private TextMeshProUGUI scoreText;

    public void SetScore(int newScore) {
        scoreText.text = "Score: " + newScore;   
    }

}