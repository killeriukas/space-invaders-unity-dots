﻿using Unity.Entities;

public struct ScoreData : IComponentData {
    public int score;
}
