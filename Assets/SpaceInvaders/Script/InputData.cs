﻿using System;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct InputData : IComponentData {
    public TimeSpan remainingCooldown;
    public bool hasInitiatedShoot;
}
