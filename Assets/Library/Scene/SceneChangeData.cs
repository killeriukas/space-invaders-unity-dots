﻿using Unity.Collections;
using Unity.Entities;

public struct SceneChangeData : IComponentData {
    public FixedString32 sceneName;
}
