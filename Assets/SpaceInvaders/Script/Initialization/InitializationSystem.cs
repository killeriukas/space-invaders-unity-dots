﻿using Unity.Entities;

[CreateAtScene("game_arena")]
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class InitializationSystem : SystemBase {

    protected override void OnCreate() {
        base.OnCreate();

        Entity uiEntity = EntityManager.CreateEntity();
        EntityManager.SetName(uiEntity, "ui_entity");
        EntityManager.AddComponentData(uiEntity, new ScoreData() { score = 0 });

        Enabled = false;
    }

    protected override void OnUpdate() {
    }

}