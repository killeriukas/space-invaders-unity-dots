﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[DisableAutoCreation]
public class MapGenerationSystem : SystemBase {

    private const int maxHeight = 100;

    private const int maxWidth = 100;


    public struct HeightData : IComponentData {
        public float value;
    }

    

    protected override void OnCreate() {
        base.OnCreate();

//        var test = EntityManager.CreateArchetype(typeof(Translation), );
    }

    protected override void OnUpdate() {

        Entity tilePrefab = PrefabConverterBehaviour.instance.GetPrefabById("tile_3d");

        NativeArray<Entity> chunkTilesArray = new NativeArray<Entity>(maxHeight * maxWidth, Allocator.Temp);

        EntityManager.Instantiate(tilePrefab, chunkTilesArray);

        for(int i = 0; i < maxHeight; ++i) {
            for(int j = 0; j < maxWidth; ++j) {
                float height = Mathf.PerlinNoise(j / (float)maxWidth, i / (float)maxHeight) * 100f - 50f;
                EntityManager.SetComponentData(chunkTilesArray[j + i * maxWidth], new Translation() { Value = new float3(j, height, i)});
            }
        }

        Enabled = false;
    }

}
