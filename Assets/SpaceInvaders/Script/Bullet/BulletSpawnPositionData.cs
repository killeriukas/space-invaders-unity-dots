﻿using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct BulletSpawnPositionData : IComponentData {
    public float3 value;
}