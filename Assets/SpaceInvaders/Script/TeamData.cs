﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct TeamData : IComponentData {
    public enum Team {
        Player,
        Enemy
    }

    public Team team;
}
