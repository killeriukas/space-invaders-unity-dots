﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

[CreateAtScene("game_arena")]
public class UISystem : SystemBase {

    protected override void OnUpdate() {

        //UI should work on events/notifications but Unity doesn't support it yet and I'm not in a mood of writing my own
        Entities.WithoutBurst().ForEach((in ScoreData scoreData) => {
            Debug.Log("Setting score: " + scoreData.score);
            UIBehaviour.instance.SetScore(scoreData.score);
        }).Run();

    }

}
