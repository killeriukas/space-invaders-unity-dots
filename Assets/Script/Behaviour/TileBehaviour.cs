﻿using UnityEngine;
using Unity.Entities;

public class TileBehaviour : MonoBehaviour, IConvertGameObjectToEntity {

    public static Entity tile3dPrefab;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        tile3dPrefab = entity;
    }


}
