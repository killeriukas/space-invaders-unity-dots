﻿using System;
using Unity.Mathematics;
using UnityEngine;

public class GameSettings : SingletonBehaviour<GameSettings> {

    [SerializeField]
    private Camera mainCamera;
    
    [SerializeField]
    private float minEnemyShootCooldown = 5f;

    [SerializeField]
    private float maxEnemyShootCooldown = 15f;


    private Unity.Mathematics.Random random;

    //these coordinates are returned at camera world position, which means if
    //z position of camera is at -10, the transformed world position shall be at -10 z as well
    //which will more than likely cause items not to be rendered, because they are out of camera's frustum
    public float3 worldMaxLowerLeft { get; private set; }
    public float3 worldMaxLowerRight { get; private set; }
    public float3 worldLowerCenter { get; private set; }



    public float3 worldMaxTopLeft { get; private set; }
    public float3 worldMaxTopRight { get; private set; }
    public float3 worldTopCenter { get; private set; }



    public const float screenPadding = 40f;

    //should be private later on
    public int maxHorizontalEnemyAmount = 5;
    public int startingEnemyAmount = 10;

    public TimeSpan playerShootCooldown { get; private set; } = TimeSpan.FromSeconds(1f);
    public TimeSpan enemyShootCooldown {
        get {
            float randCooldown = random.NextFloat(minEnemyShootCooldown, maxEnemyShootCooldown);
            return TimeSpan.FromSeconds(randCooldown);
        }
    }

    protected override void Awake() {
        base.Awake();
        random = new Unity.Mathematics.Random(123456u);

        float maxLeft = screenPadding;
        float maxRight = Screen.width - screenPadding;

        float maxBottom = screenPadding;
        float maxTop = Screen.height - screenPadding;

        float centerHorizontal = Screen.width * 0.5f;

        worldMaxLowerLeft = mainCamera.ScreenToWorldPoint(new Vector3(maxLeft, maxBottom, 0f));
        worldMaxLowerRight = mainCamera.ScreenToWorldPoint(new Vector3(maxRight, maxBottom, 0f));
        worldLowerCenter = mainCamera.ScreenToWorldPoint(new Vector3(centerHorizontal, maxBottom, 0f));

        worldMaxTopLeft = mainCamera.ScreenToWorldPoint(new Vector3(maxLeft, maxTop, 0f));
        worldMaxTopRight = mainCamera.ScreenToWorldPoint(new Vector3(maxRight, maxTop, 0f));
        worldTopCenter = mainCamera.ScreenToWorldPoint(new Vector3(centerHorizontal, maxTop, 0f));
    }

}
