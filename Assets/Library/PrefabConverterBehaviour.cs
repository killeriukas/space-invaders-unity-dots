﻿using UnityEngine;
using Unity.Entities;
using System;
using System.Collections.Generic;

public class PrefabConverterBehaviour : SingletonBehaviour<PrefabConverterBehaviour>, IConvertGameObjectToEntity, IDeclareReferencedPrefabs {

#pragma warning disable 0649
    [Serializable]
    private struct IdToPrefab {
        public string id;
        public GameObject go;
    }

    [SerializeField]
    private IdToPrefab[] idToPrefabArray;
#pragma warning restore 0649

    private Dictionary<string, Entity> entityPrefabById = new Dictionary<string, Entity>();

    public Entity GetPrefabById(string id) => entityPrefabById[id];

    protected override void OnDestroy() {
        entityPrefabById.Clear();
        base.OnDestroy();
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        for(int i = 0; i < idToPrefabArray.Length; ++i) {
            IdToPrefab mapping = idToPrefabArray[i];
            Entity convertedEntity = conversionSystem.GetPrimaryEntity(mapping.go);
            entityPrefabById.Add(mapping.id, convertedEntity);
        }
    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
        for(int i = 0; i < idToPrefabArray.Length; ++i) {
            referencedPrefabs.Add(idToPrefabArray[i].go);
        }
    }

}
