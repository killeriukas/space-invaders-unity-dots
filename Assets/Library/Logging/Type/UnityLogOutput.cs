﻿using UnityEngine;

namespace TMI.LogManagement {

    public class UnityLogOutput : ILogOutput {

        public void Log(string categoryType, string message) {
            Debug.Log(message);
        }

        public void Log(ILoggable loggable, string message) {
            Debug.Log(message);
        }

        public void LogWarning(string categoryType, string message) {
            Debug.LogWarning(message);
        }

        public void LogWarning(ILoggable loggable, string message) {
            Debug.LogWarning(message);
        }

        public void LogError(ILoggable loggable, string message) {
            Debug.LogError(message);
        }

    }

}