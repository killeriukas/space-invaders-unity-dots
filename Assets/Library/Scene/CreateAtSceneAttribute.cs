﻿using System;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class CreateAtSceneAttribute : Attribute {

    public string sceneName { get; private set; }

    public CreateAtSceneAttribute(string sceneName) {
        this.sceneName = sceneName;
    }

}