﻿using System;

namespace TMI.LogManagement {

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
    public class LoggableGroupAttribute : Attribute {

        public string groupName { get; private set; }

        public LoggableGroupAttribute(string groupName) {
            this.groupName = groupName;
        }

    }

}