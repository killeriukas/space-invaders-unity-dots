﻿using Unity.Entities;

public struct BulletMovementDirectionData : IComponentData {
    public float verticalDirection;
}
