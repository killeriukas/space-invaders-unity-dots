﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class EnemyMovementSystem : SystemBase {

    private enum Direction {
        Left,
        Right
    }

    private Direction currentDirection;
    private Random random;
    //private TimeSpan

    protected override void OnCreate() {
        base.OnCreate();
        random = new Random(5456515u);
        bool isLeft = random.NextBool();
        currentDirection = isLeft ? Direction.Left : Direction.Right;
    }

    protected override void OnUpdate() {     
        float dt = Time.DeltaTime;

        float maxLeft = GameSettings.instance.worldMaxTopLeft.x;
        float maxRight = GameSettings.instance.worldMaxTopRight.x;

        //if() {
        
        //}


        Direction direction = currentDirection;

        Entities.WithAll<EnemyShipTag>().ForEach((ref Translation translation, in SpeedData speedData) => {
            float3 currentPosition = translation.Value;

            switch(direction) {
                case Direction.Left:
                    currentPosition.x -= speedData.value * dt;

                    if(currentPosition.x < maxLeft) {
                        currentPosition.x = maxLeft;
                    }

                    break;
                case Direction.Right:
                    currentPosition.x += speedData.value * dt;

                    if(currentPosition.x > maxRight) {
                        currentPosition.x = maxRight;
                    }

                    break;
            }

            translation.Value = currentPosition;
        }).Run();

    }
}
