﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

[CreateAtScene("game_arena")]
public class BulletCollisionSystem : ECBSystem<EndSimulationEntityCommandBufferSystem> {

    [BurstCompile]
    //[RequireComponentTag(typeof(BulletTag))] //doesn't seem to work on Trigger jobs
    private struct BulletCollisionJob : ITriggerEventsJob {

        public EntityCommandBuffer ecb;

        [ReadOnly]
        public ComponentDataFromEntity<BulletTag> bulletEntities;

        [ReadOnly]
        public ComponentDataFromEntity<TeamData> teamEntities;

        public void Execute(TriggerEvent triggerEvent) {
            Entity ea = triggerEvent.Entities.EntityA;
            Entity eb = triggerEvent.Entities.EntityB;

            //if at least one of the components don't have team, that means they must collide
            if(teamEntities.HasComponent(ea) && teamEntities.HasComponent(eb)) {

                TeamData.Team teamA = teamEntities[ea].team;
                TeamData.Team teamB = teamEntities[eb].team;

                //ignore bullets hitting the spawner
                if(teamA == teamB) {
                    return;
                }

            }

            if(bulletEntities.HasComponent(ea)) {
                //spawn particles for bullet hit somehow
                ecb.DestroyEntity(ea);
            }

            if(bulletEntities.HasComponent(eb)) {
                //spawn particles for bullet hit somehow
                ecb.DestroyEntity(eb);
            }

        }

    }

    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate() {
        base.OnCreate();
        buildPhysicsWorld = World.GetExistingSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetExistingSystem<StepPhysicsWorld>();
    }

    protected override void OnUpdate() {
        BulletCollisionJob bulletCollisionJob = new BulletCollisionJob();
        bulletCollisionJob.bulletEntities = GetComponentDataFromEntity<BulletTag>(true);
        bulletCollisionJob.teamEntities = GetComponentDataFromEntity<TeamData>(true);
        bulletCollisionJob.ecb = CreateEntityCommandBuffer();
        JobHandle bulletCollisionHandle = bulletCollisionJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        AddJobHandleForProducer(bulletCollisionHandle);

        Dependency = bulletCollisionHandle;

        //complete the job here, because it crashes for no known reason...
        //maybe it's a bug in the SDK, but need to investigate further
        Dependency.Complete();
    }
}
