﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[CreateAtScene("game_arena")]
public class EnemySpawnSystem : SystemBase {

    private enum State {
        Start,
        Gameplay
    }

    private State currentState;

    protected override void OnCreate() {
        base.OnCreate();
        currentState = State.Start;
    }

    protected override void OnUpdate() {

        switch(currentState) {
            case State.Start:
                UpdateStartState();
                currentState = State.Gameplay;
                break;
            case State.Gameplay:

                break;
        }

    }


    private void UpdateStartState() {
        Entity enemyPrefab = PrefabConverterBehaviour.instance.GetPrefabById("enemy_ship");

        float3 topCenter = GameSettings.instance.worldTopCenter;

        int startingEnemyAmount = GameSettings.instance.startingEnemyAmount;
        int maxHorizontal = GameSettings.instance.maxHorizontalEnemyAmount;

        int minHorizontal = math.min(maxHorizontal, startingEnemyAmount) - 1;

        const float paddingBetweenShips = 20f;
        float maxLeft = topCenter.x - (minHorizontal * 0.5f) * paddingBetweenShips;


        NativeArray<Entity> newEntities = new NativeArray<Entity>(startingEnemyAmount, Allocator.Temp);
        EntityManager.Instantiate(enemyPrefab, newEntities);
        
        float yMultiplier = 0f;
        int column = 0;
        for(int i = 0; i < newEntities.Length; ++i) {

            if(i > 0 && i % maxHorizontal == 0) {
                yMultiplier += paddingBetweenShips;
                column = 0;
            }

            EntityManager.SetComponentData(newEntities[i], new Translation() { Value = new float3(maxLeft + column * paddingBetweenShips, topCenter.y - yMultiplier, 0f) });

            TimeSpan cooldown = GameSettings.instance.enemyShootCooldown;

            EntityManager.SetComponentData(newEntities[i], new InputData() { hasInitiatedShoot = false, remainingCooldown = cooldown });
            column++;
        }

        newEntities.Dispose();
    }

}
