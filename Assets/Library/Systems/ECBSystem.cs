﻿using Unity.Entities;
using Unity.Jobs;

public abstract class ECBSystem<TECB> : SystemBase where TECB : EntityCommandBufferSystem {

    private TECB ecb;

    protected override void OnCreate() {
        base.OnCreate();
        ecb = World.GetExistingSystem<TECB>();
    }

    protected EntityCommandBuffer CreateEntityCommandBuffer() {
        return ecb.CreateCommandBuffer();
    }

    protected void AddJobHandleForProducer(JobHandle jobHandle) {
        ecb.AddJobHandleForProducer(jobHandle);
    }

}