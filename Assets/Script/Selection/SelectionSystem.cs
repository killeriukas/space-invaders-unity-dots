﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

[DisableAutoCreation]
public class SelectionSystem : SystemBase {

    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate() {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
    }


    protected override void OnUpdate() {
        NativeList<Entity> selectedEntities = new NativeList<Entity>(4, Allocator.TempJob);
        
        NativeList<Entity>.ParallelWriter parallelWriter = selectedEntities.AsParallelWriter();

        Dependency = Entities.ForEach((Entity entity, in Translation translation) => {

            if(translation.Value.x < 0) {
                parallelWriter.AddNoResize(entity);
            }

        }).ScheduleParallel(Dependency);


        //    var commandBuffer = endSimulationEntityCommandBufferSystem.CreateCommandBuffer();

        //      EntityQuery test;
        //        commandBuffer.AddComponent()


        Dependency.Complete();

        Debug.Log("selected entities: " + selectedEntities.Length);
        selectedEntities.Dispose();

     //   Dependency = selectedEntities.Dispose(Dependency);
    }




}
