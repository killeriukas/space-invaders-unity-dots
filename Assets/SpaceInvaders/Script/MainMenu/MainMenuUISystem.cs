﻿using Unity.Entities;

[CreateAtScene("main_menu")]
public class MainMenuUISystem : SystemBase {

    private SceneChangeSystem sceneChangeSystem;

    protected override void OnCreate() {
        base.OnCreate();
        sceneChangeSystem = World.GetExistingSystem<SceneChangeSystem>();
    }

    protected override void OnStartRunning() {
        base.OnStartRunning();
        MainMenuUIBehaviour.instance.startGameButton.onClick.AddListener(ChangeScene);
    }

    private void ChangeScene() {
        sceneChangeSystem.ChangeScene("game_arena");
    }

    protected override void OnUpdate() {
    }

    protected override void OnStopRunning() {
        MainMenuUIBehaviour.instance.startGameButton.onClick.RemoveListener(ChangeScene);
        base.OnStopRunning();
    }

}
