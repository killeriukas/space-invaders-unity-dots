﻿using System;
using Unity.Entities;
using UnityEngine;

[CreateAtScene("game_arena")]
[UpdateInGroup(typeof(InputSystemGroup))]
public class PlayerInputSystem : SystemBase {

    protected override void OnUpdate() {
        bool isSpaceDown = Input.GetKeyDown(KeyCode.Space);
        TimeSpan dt = TimeSpan.FromSeconds(Time.DeltaTime);
        TimeSpan coolDown = GameSettings.instance.playerShootCooldown;

        Entities.WithAll<PlayerShipTag>().ForEach((Entity entity, ref InputData inputData) => {

            if(inputData.remainingCooldown > TimeSpan.Zero) {
                inputData.remainingCooldown -= dt;
            } else {
                if(isSpaceDown) {
                    inputData.remainingCooldown = coolDown;
                    inputData.hasInitiatedShoot = true;
                }
            }

        }).ScheduleParallel();

    }

}