﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[CreateAtScene("game_arena")]
public class BulletSpawnSystem : ECBSystem<BeginInitializationEntityCommandBufferSystem> {

    protected override void OnUpdate() {

        Entity bulletPrefab = PrefabConverterBehaviour.instance.GetPrefabById("bullet");

        var ecb = CreateEntityCommandBuffer().ToConcurrent();

        JobHandle jobHandle = Entities.ForEach((Entity entity,
            int entityInQueryIndex,
            ref InputData inputData,
            in Translation translation,
            in BulletSpawnPositionData bulletSpawnPositionData,
            in TeamData entityTeam) => {

            if(inputData.hasInitiatedShoot) {
                inputData.hasInitiatedShoot = false;

                float3 spawnPosition = translation.Value + bulletSpawnPositionData.value;

                Entity newBullet = ecb.Instantiate(entityInQueryIndex, bulletPrefab);
                ecb.SetComponent(entityInQueryIndex, newBullet, new Translation() { Value = spawnPosition });


                float3 dir = math.normalize(spawnPosition - translation.Value);
                float ySign = math.sign(dir.y);

                ecb.AddComponent(entityInQueryIndex, newBullet, new BulletMovementDirectionData() { verticalDirection = ySign });
                ecb.AddComponent(entityInQueryIndex, newBullet, new TeamData() { team = entityTeam.team });
            }

        }).ScheduleParallel(Dependency);

        AddJobHandleForProducer(jobHandle);

        Dependency = jobHandle;
    }


}
