﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[CreateAtScene("game_arena")]
public class PlayerMovementSystem : SystemBase {

    protected override void OnUpdate() {
        
        float dt = Time.DeltaTime;
        float horizontalInput = Input.GetAxis("Horizontal");

        float maxLeft = GameSettings.instance.worldMaxLowerLeft.x;
        float maxRight = GameSettings.instance.worldMaxLowerRight.x;

        Entities.WithAll<PlayerShipTag>().ForEach((ref Translation translation, in SpeedData speedData) => {
            float3 currentPosition = translation.Value;

            currentPosition.x += horizontalInput * speedData.value * dt;

            if(currentPosition.x < maxLeft) {
                currentPosition.x = maxLeft;
            }

            if(currentPosition.x > maxRight) {
                currentPosition.x = maxRight;
            }

            translation.Value = currentPosition;
        }).ScheduleParallel();

    }
}
