﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;

[CreateAtScene("game_arena")]
public class BulletMovementSystem : SystemBase {
    

    protected override void OnUpdate() {
        float3 linearForce = new float3(0f, 50f, 0f);

        Entities.WithAll<BulletTag>().ForEach((ref PhysicsVelocity velocity, in BulletMovementDirectionData bulletMovementDirectionData) => {
            velocity.Linear = bulletMovementDirectionData.verticalDirection * linearForce;
        }).ScheduleParallel();

    }
}

