﻿using System;
using Unity.Entities;
using UnityEngine;

public class EnemyInputSystem : SystemBase {

    protected override void OnUpdate() {
        
        TimeSpan dt = TimeSpan.FromSeconds(Time.DeltaTime);
        TimeSpan coolDown = GameSettings.instance.enemyShootCooldown; //the same random cooldown will apply to all entities, not individually yet

        Entities.WithAll<EnemyShipTag>().ForEach((Entity entity, ref InputData inputData) => {

            if(inputData.remainingCooldown > TimeSpan.Zero) {
                inputData.remainingCooldown -= dt;
            } else {
                inputData.remainingCooldown = coolDown;
                inputData.hasInitiatedShoot = true;
            }

        }).ScheduleParallel();
        
    }

}