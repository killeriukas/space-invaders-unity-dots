﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

public class PlayerCollisionSystem : ECBSystem<EndSimulationEntityCommandBufferSystem> {

    [BurstCompile]
    private struct PlayerCollisionJob : ITriggerEventsJob {

        public EntityCommandBuffer ecb;

        [ReadOnly]
        public ComponentDataFromEntity<PlayerShipTag> playerEntities;

        [ReadOnly]
        public ComponentDataFromEntity<TeamData> teamEntities;

        public void Execute(TriggerEvent triggerEvent) {
            Entity ea = triggerEvent.Entities.EntityA;
            Entity eb = triggerEvent.Entities.EntityB;

            //if at least one of the components don't have team, that means they must collide
            if(teamEntities.HasComponent(ea) && teamEntities.HasComponent(eb)) {

                TeamData.Team teamA = teamEntities[ea].team;
                TeamData.Team teamB = teamEntities[eb].team;

                //ignore bullets hitting the spawner
                if(teamA == teamB) {
                    return;
                }

            }

            if(playerEntities.HasComponent(ea)) {
                ecb.DestroyEntity(ea);
            }

            if(playerEntities.HasComponent(eb)) {
                ecb.DestroyEntity(eb);
            }

        }

    }

    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate() {
        base.OnCreate();
        buildPhysicsWorld = World.GetExistingSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetExistingSystem<StepPhysicsWorld>();
    }

    protected override void OnUpdate() {
        PlayerCollisionJob playerCollisionJob = new PlayerCollisionJob();
        playerCollisionJob.playerEntities = GetComponentDataFromEntity<PlayerShipTag>(true);
        playerCollisionJob.teamEntities = GetComponentDataFromEntity<TeamData>(true);
        playerCollisionJob.ecb = CreateEntityCommandBuffer();
        JobHandle playerCollisionHandle = playerCollisionJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        AddJobHandleForProducer(playerCollisionHandle);

        Dependency = playerCollisionHandle;

        //complete the job here, because it crashes for no known reason...
        //maybe it's a bug in the SDK, but need to investigate further
        Dependency.Complete();
    }
}
